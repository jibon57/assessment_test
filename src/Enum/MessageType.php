<?php

namespace Jibon\AssessmentTest\Enum;

enum MessageType
{
    case System;
    case Manual;
}
