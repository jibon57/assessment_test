<?php

namespace Jibon\AssessmentTest\Enum;

enum UserType
{
    case Teacher;
    case Parent;
    case Student;
}
