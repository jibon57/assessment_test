<?php

namespace Jibon\AssessmentTest;

use Jibon\AssessmentTest\Enum\MessageType;
use Jibon\AssessmentTest\Enum\UserType;
use Jibon\AssessmentTest\Response\SaveMessageResponse;
use Jibon\AssessmentTest\User\User;

/**
 *
 */
class Messenger
{
    /**
     * @var User
     */
    private User $sender;
    /**
     * @var User
     */
    private User $receiver;
    /**
     * @var string
     */
    private string $message;
    /**
     * @var int
     */
    private int $creationTime;
    /**
     * @var MessageType
     */
    private MessageType $messageType;

    /**
     * @param User $sender
     * @param User $receiver
     * @param string $message
     * @param MessageType $messageType
     * @param int $creationTime
     */
    public function __construct(
        User        $sender,
        User        $receiver,
        string      $message,
        MessageType $messageType,
        int         $creationTime
    )
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->message = $message;
        $this->messageType = $messageType;
        $this->creationTime = $creationTime;
    }

    /**
     * @return User
     */
    public function getSender(): User
    {
        return $this->sender;
    }

    /**
     * @param User $sender
     */
    public function setSender(User $sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return User
     */
    public function getReceiver(): User
    {
        return $this->receiver;
    }

    /**
     * @param User $receiver
     */
    public function setReceiver(User $receiver): void
    {
        $this->receiver = $receiver;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getCreationTime(string $format = "Y/m/d H:i:s"): string
    {
        return date($format, $this->creationTime);
    }

    /**
     * @return MessageType
     */
    public function getMessageType(): MessageType
    {
        return $this->messageType;
    }

    /**
     * @param MessageType $messageType
     */
    public function setMessageType(MessageType $messageType): void
    {
        $this->messageType = $messageType;
    }

    /**
     * @return SaveMessageResponse
     */
    public function save()
    {
        $output = new SaveMessageResponse();
        $validate = $this->validateFields();

        if (!$validate->getStatus()) {
            $output->setMsg($validate->getMsg());
            return $output;
        }

        $output->setStatus(true);
        $output->setMsg("successfully saved");

        return $output;
    }

    /** This method will validate all fields
     * @return SaveMessageResponse
     */
    private function validateFields()
    {
        $output = new SaveMessageResponse();

        // message can't be empty
        if (empty($this->message)) {
            $output->setMsg("message required");
            return $output;
        }

        // check whom to send system message
        if (
            $this->messageType === MessageType::System &&
            $this->sender->getUserType() !== UserType::Teacher
        ) {
            $output->setMsg("System messages can only be sent from Teachers and only to Students");
            return $output;
        }

        // check whom to send message
        if (
            $this->receiver->getUserType() !== UserType::Teacher &&
            (
                $this->sender->getUserType() === UserType::Student ||
                $this->sender->getUserType() === UserType::Parent
            )
        ) {
            $output->setMsg("You can only send message to teacher");
            return $output;
        }

        $output->setStatus(true);
        return $output;
    }
}
