<?php

namespace Jibon\AssessmentTest\Response;

/**
 *
 */
abstract class BaseResponse
{
    /**
     * @var bool
     */
    private $status = false;
    /**
     * @var string
     */
    private $msg = "";

    public function __construct()
    {
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getMsg(): string
    {
        return $this->msg;
    }

    /**
     * @param string $msg
     */
    public function setMsg(string $msg): void
    {
        $this->msg = $msg;
    }

}
