<?php

namespace Jibon\AssessmentTest\User;

use Jibon\AssessmentTest\Enum\UserType;

/**
 *
 */
class ParentUser extends User
{
    /**
     * @param string $userId
     * @param string $salutation
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string|null $avatar
     */
    public function __construct(
        string $userId,
        string $salutation,
        string $firstName,
        string $lastName,
        string $email,
        string $avatar = null
    )
    {
        parent::__construct($userId, $firstName, $lastName, $email, $avatar);
        $this->salutation = $salutation;
        $this->setUserType(UserType::Parent);
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        if (!empty($this->salutation)) {
            return sprintf("%s %s %s", $this->salutation, $this->getFirstName(), $this->getLastName());
        }

        return sprintf("%s %s", $this->getFirstName(), $this->getLastName());
    }
}
