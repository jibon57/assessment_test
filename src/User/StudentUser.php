<?php

namespace Jibon\AssessmentTest\User;

use Jibon\AssessmentTest\Enum\UserType;

/**
 *
 */
class StudentUser extends User
{
    /**
     * @param string $userId
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string|null $avatar
     */
    public function __construct(
        string $userId,
        string $firstName,
        string $lastName,
        string $email,
        string $avatar = null
    )
    {
        parent::__construct($userId, $firstName, $lastName, $email, $avatar);
        $this->setUserType(UserType::Student);
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return sprintf("%s %s", $this->getFirstName(), $this->getLastName());
    }
}
