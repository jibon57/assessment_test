<?php

namespace Jibon\AssessmentTest\User;

use Jibon\AssessmentTest\Enum\UserType;
use Jibon\AssessmentTest\Response\SaveUserResponse;

abstract class User
{
    /**
     * @var string
     */
    protected string $salutation;
    /**
     * @var string
     */
    protected string $firstName;
    /**
     * @var string
     */
    protected string $lastName;
    /**
     * @var string
     */
    private string $userId;
    /**
     * @var string
     */
    private string $email;
    /**
     * @var string
     */
    private string $avatar;
    /**
     * @var UserType
     */
    private UserType $userType;

    /**
     * @param string $userId
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string|null $avatar
     */
    public function __construct(
        string $userId,
        string $firstName,
        string $lastName,
        string $email,
        string $avatar = null
    )
    {
        $this->userId = $userId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        if ($avatar) {
            $this->avatar = $avatar;
        }
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        if (empty($this->avatar)) {
            return "https://mydomain.com/default.jpg";
        }
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar(string $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return UserType
     */
    public function getUserType(): UserType
    {
        return $this->userType;
    }

    /**
     * @param UserType $userType
     */
    public function setUserType(UserType $userType): void
    {
        $this->userType = $userType;
    }

    /**
     * @return SaveUserResponse
     */
    public function save()
    {
        $output = new SaveUserResponse();
        $validate = $this->validateFields();

        if (!$validate->getStatus()) {
            $output->setMsg($validate->getMsg());
            return $output;
        }

        $output->setStatus(true);
        $output->setMsg("successfully saved");

        return $output;
    }

    /** This method will validate all fields
     * @return SaveUserResponse
     */
    private function validateFields()
    {
        $output = new SaveUserResponse();

        // we'll check if userId is empty
        if (empty($this->userId)) {
            $output->setMsg("userId required");
            return $output;
        }

        // we'll check if email is empty
        if (empty($this->email)) {
            $output->setMsg("email required");
            return $output;
        }

        // both firstname & lastname are required
        if (empty($this->firstName) || empty($this->lastName)) {
            $output->setMsg("both first name & last name required");
            return $output;
        }

        // validate if the email is actual email
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $output->setMsg("email isn't valid");
            return $output;
        }

        // make sure avatar is in correct format
        if (
            !empty($this->avatar) &&
            !preg_match("/\.(gif|png|bmp|jpe?g)$/", $this->avatar)
        ) {
            $output->setMsg("profile picture isn't valid");
            return $output;
        }

        $output->setStatus(true);
        return $output;
    }
}
