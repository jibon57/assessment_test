<?php


use Jibon\AssessmentTest\Enum\MessageType;
use Jibon\AssessmentTest\Enum\UserType;
use Jibon\AssessmentTest\Messenger;
use Jibon\AssessmentTest\User\ParentUser;
use Jibon\AssessmentTest\User\StudentUser;
use Jibon\AssessmentTest\User\TeacherUser;
use PHPUnit\Framework\TestCase;

class MessengerTest extends TestCase
{
    /**
     * @var TeacherUser
     */
    private TeacherUser $teacher;
    /**
     * @var StudentUser
     */
    private StudentUser $student;
    /**
     * @var ParentUser
     */
    private ParentUser $parent;

    public function testSendMessageToStudentFromTeacher()
    {
        $message = new Messenger($this->teacher, $this->student, "Hello there!", MessageType::System, time());

        $this->assertSame(true, $message->save()->getStatus(), $message->save()->getMsg());

        // match with receiver
        $this->assertSame($message->getReceiver()->getUserId(), $this->student->getUserId());

        // validate format of CreationTime
        $this->assertSame(true, (bool)strtotime($message->getCreationTime()));
    }

    public function testSendMessageFromStudentToTeacher()
    {
        $message = new Messenger($this->student, $this->parent, "Hello there!", MessageType::System, time());

        // won't save as student can't System
        $this->assertSame(false, $message->save()->getStatus(), $message->save()->getMsg());

        $message->setMessageType(MessageType::Manual);
        // won't save as student only send message to teacher
        $this->assertSame(false, $message->save()->getStatus(), $message->save()->getMsg());

        $message->setReceiver($this->teacher);
        $this->assertSame(true, $message->save()->getStatus(), $message->save()->getMsg());

        // match with receiver
        $this->assertSame($message->getReceiver()->getUserId(), $this->teacher->getUserId());
    }

    public function testSendMessageFromParentToTeacher()
    {
        $message = new Messenger($this->parent, $this->student, "Hello there!", MessageType::System, time());

        // won't save as can't send System message
        $this->assertSame(false, $message->save()->getStatus(), $message->save()->getMsg());

        $message->setMessageType(MessageType::Manual);
        // won't save as only send message to teacher
        $this->assertSame(false, $message->save()->getStatus(), $message->save()->getMsg());

        $message->setReceiver($this->teacher);
        $this->assertSame(true, $message->save()->getStatus(), $message->save()->getMsg());

        // match with receiver
        $this->assertSame($message->getReceiver()->getUserId(), $this->teacher->getUserId());
    }

    public function testRetrieveFromUserObject()
    {
        $this->assertSame("student01", $this->student->getUserId());
        $this->assertSame("student01@example.com", $this->student->getEmail());
        $this->assertEquals(UserType::Student, $this->student->getUserType());

        $this->assertSame("parent01", $this->parent->getUserId());
        $this->assertSame("parent@example.com", $this->parent->getEmail());
        $this->assertNotSame(UserType::Teacher, $this->parent->getUserType());
    }

    protected function setUp(): void
    {
        $teacher = new TeacherUser("teacher01", "Mr.","Teacher", "01", "teacher01@example.com");
        $this->teacher = $teacher;

        $parent = new ParentUser("parent01", "Mr.","Parent", "01", "parent@example.com");
        $this->parent = $parent;

        $student = new StudentUser("student01", "Student", "01", "student01@example.com");
        $this->student = $student;
    }
}
