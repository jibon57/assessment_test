<?php

use Jibon\AssessmentTest\Enum\UserType;
use Jibon\AssessmentTest\User\StudentUser;
use Jibon\AssessmentTest\User\TeacherUser;
use PHPUnit\Framework\TestCase;

class UsersTest extends TestCase
{
    public function testAddStudent()
    {
        $student = new StudentUser("std01", "Student", "01", "student@example.com");
        $this->assertSame("Student 01", $student->getFullName());

        // can't save because user type required
        $this->assertSame(UserType::Student, $student->getUserType(), $student->save()->getMsg());

        $student->setAvatar("https://mydomain.com/test.txt");
        // can't save because invalid profile avatar
        $this->assertSame(false, $student->save()->getStatus(), $student->save()->getMsg());
        $student->setAvatar("https://mydomain.com/test.jpg");

        $this->assertSame(true, $student->save()->getStatus(), $student->save()->getMsg());
    }

    public function testAddTeacher()
    {
        $teacher = new TeacherUser("tea01", "Mr.","Teacher", "01", "teacher01@example.com");

        $this->assertSame(UserType::Teacher, $teacher->getUserType());

        $teacher->setAvatar("https://mydomain.com/test.txt");
        // can't save because invalid profile avatar
        $this->assertSame(false, $teacher->save()->getStatus(), $teacher->save()->getMsg());
        $teacher->setAvatar("https://mydomain.com/test.jpg");

        $this->assertSame(true, $teacher->save()->getStatus(), $teacher->save()->getMsg());
    }
}
